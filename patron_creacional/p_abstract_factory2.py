"""
El abstract factory es una generalización del factory method. 
Es un grupo de factory methods , donde cada uno es responsable de generar 
un tipo diferente de objeto.
https://github.com/FactoryBoy/factory_boy

"""


class Frog:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def interact_with(self, obstacle):
        act = obstacle.action()
        msg = f'{self} the frog encounters {obstacle} and {act}'
        print(msg)


class Bug:
    def __str__(self):
        return 'a bug'

    def action(self):
        return 'eats it'


# Abstract factory
class FrogWorld:
    def __init__(self, name):
        print(self)
        self.player_name = name

    def __str__(self):
        return '\n\n\t ----Frog World ---'

    def make_character(self):
        return Frog(self.player_name)

    def make_obstacle(self):
        return Bug()


class Wizard:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def interact_with(self, obstacle):
        act = obstacle.action()
        msg = f'{self} the Wizard battles agains {obstacle} and {act}!'
        print(msg)


class Ork:
    def __str__(self):
        return 'an evil ork'


class WizardWorld:
    def __init__(self, name):
        print(self)
        self.player_name = name

    def __str__(self):
        return '\n\n\t ----Wizard World ---'

    def make_character(self):
        return Wizard(self.player_name)

    def make_obstacle(self):
        return Ork()


class GameEnviroment:
    def __init__(self, factory):
        self.hero = factory.make_character()
        self.obstacle = factory.make_obstacle()

    def play(self):
        self.hero.interact_with(self.obstacle)


def validate_age(name):
    try:
        age = input(f'Welcome {name}. How old are you?  ')
        age = int(age)
    except ValueError as e:
        print(f'Age {age} is invalid, please try again')
        return (False, age)
    return (True, age)


def main():
    name = input("Hello. What's your name ? ")
    valid_input = False
    while not valid_input:
        valid_input, age = validate_age(name)

    game = FrogWorld if age < 18 else WizardWorld
    enviroment = GameEnviroment(game(name))
    enviroment.play()
