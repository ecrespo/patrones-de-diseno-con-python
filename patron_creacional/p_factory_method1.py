#
"""
patrón creacional 
factory method
Intención: Definir una interfaz para la creación de un objeto, permitiendo que las subclases decidan que tipo de objeto instanciar 
Aplicabilidad: se suele usar no puedes anticipar el tipo de objeto final que necesitas instanciar o cuando quieres especificar en subclases como
instanciar ese objeto. 
Participantes: 
* Product: Interfaz común a todos los resultados de la instanciación 
* ConcreteProduct: Resultado de un ConcreteCreator (implementa la interfaz Product)
* Creator: Clase abstracta para instanciar un Product de forma genérica 
* ConcreteCreator: La subclase que sobreescribiendo parte de Creator instancia un ConcreteProduct 
https://stackabuse.com/the-factory-method-design-pattern-in-python
https://refactoring.guru/es/design-patterns/factory-method/python/example
https://refactoring.guru/es/design-patterns/factory-method
"""

import abc


class Shape(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def calculate_area(self):
        pass

    @abc.abstractmethod
    def calculate_perimeter(self):
        pass


class Rectangle(Shape):
    def __init__(self, height, width):
        self.height = height
        self.width = width

    def calculate_area(self):
        return self.height * self.width

    def calculate_perimeter(self):
        return 2 * (self.height + self.width)


class Square(Shape):
    def __init__(self, width):
        self.width = width

    def calculate_area(self):
        return self.width ** 2

    def calculate_perimeter(self):
        return 4 * self.width


class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def calculate_area(self):
        return 3.14 * self.radius * self.radius

    def calculate_perimeter(self):
        return 2 * 3.14 * self.radius


class ShapeFactory:
    def create_shape(self, name):
        if name == 'circle':
            radius = input("Enter the radius of the circle: ")
            return Circle(float(radius))

        elif name == 'rectangle':
            height = input("Enter the height of the rectangle: ")
            width = input("Enter the width of the rectangle: ")
            return Rectangle(int(height), int(width))

        elif name == 'square':
            width = input("Enter the width of the square: ")
            return Square(int(width))


def shapes_client():
    shape_factory = ShapeFactory()
    shape_name = input("Enter the name of the shape: ")

    shape = shape_factory.create_shape(shape_name)

    print(f"The type of object created: {type(shape)}")
    print(f"The area of the {shape_name} is: {shape.calculate_area()}")
    print(
        f"The perimeter of the {shape_name} is: {shape.calculate_perimeter()}")


if __name__ == "__main__":
    shapes_client()
