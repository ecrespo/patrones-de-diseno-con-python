#
"""
patrón creacional 
factory method
Se basa en una simple función escrita para manejar la tarea de la creación del objeto . 

Lo ejecutamos pasando un parámetro que provee información acerca de lo que queremos, 
como resultado se crea el objeto.

En el mundo del software django usa el método factoria para crear campos en un formulario web. 
El módulo form soporta la creación de diferentes tipos de campos , y una parte de su comportamiento 
puede ser personalizado usando atributos como max_length o required. 

from django import forms


class PersonForms(forms.Form):
    name = forms.CharField(max_length=100)
    birth_date = forms.DateField(required=True)


"""
import json
import xml.etree.ElementTree as etree


class JSONDataExtractor:
    def __init__(self, filepath):
        self.data = dict()
        with open(filepath, mode='r', encoding='utf-8') as f:
            self.data = json.load(f)

    @property
    def parsed_data(self):
        return self.data


class XMLDataExtractor:

    def __init__(self, filepath):
        self.tree = etree.parse(filepath)

    @property
    def parsed_data(self):
        return self.tree


def dataextraction_factory(filepath):
    if filepath.endswith('json'):
        extractor = JSONDataExtractor
    elif filepath.endswith('xml'):
        extractor = XMLDataExtractor
    else:
        raise ValueError(f"Cannot extract data from {filepath}")


def extract_data_from(filepath):
    factory_obj = None
    try:
        factory_obj = dataextraction_factory(filepath)
    except ValueError as e:
        print(e)
    return factory_obj


def main():
    sqlite_factory = extract_data_from("data/person.sq3")
    print()


if __name__ == '__main__':
    json_factory = extract_data_from("data/movies.json")
    json_data = json_factory.parsed_data()
    print(len(json_data))
    for movie in json_data:
        print(movie)

    xml_factory = extract_data_from("data/person.xml")
    xml_data = xml_factory.parsed_data()
    liars = xml_data.findall(f".//person[lastName='Liar']")
    print(len(liars))
    for liar in liars:
        print(liar)
