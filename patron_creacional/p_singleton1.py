# patrón creacional
# patrón creacional


"""
Patrón creacional
Singleton
Intención: Asegurar que la clase sólo tiene una instanciación 
Se suele usar no quieres repetir la instanciación de un objeto varias veces durante la aplicación por que puede 
acarrear compllicaciones (rendimiento, disponibilidad)

1. Lectura de archivos.
2. Instanciando conexiones a base de datos. 
3. COmo base de datos en memoria 

https://refactoring.guru/es/design-patterns/singleton
https://refactoring.guru/es/design-patterns/singleton/python/example#lang-features


"""


class SingletonGovt:
    __instance__ = None

    def __init__(self):
        """ Constructor.
        """
        if SingletonGovt.__instance__ is None:
            SingletonGovt.__instance__ = self
        else:
            raise Exception("You cannot create another SingletonGovt class")

    @staticmethod
    def get_instance():
        """ Static method to fetch the current instance.
        """
        if not SingletonGovt.__instance__:
            SingletonGovt()
        return SingletonGovt.__instance__


if __name__ == "__main__":

    government = SingletonGovt()
    print(government)

    same_government = SingletonGovt.get_instance()
    print(same_government)

    another_government = SingletonGovt.get_instance()
    print(another_government)

    new_government = SingletonGovt()
    print(new_government)
