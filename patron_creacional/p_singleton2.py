# patrón creacional
# patrón creacional


"""
Patrón creacional
Singleton
Intención: Asegurar que la clase sólo tiene una instanciación 
Se suele usar no quieres repetir la instanciación de un objeto varias veces durante la aplicación por que puede 
acarrear compllicaciones (rendimiento, disponibilidad)

1. Lectura de archivos.
2. Instanciando conexiones a base de datos. 
3. COmo base de datos en memoria 

https://refactoring.guru/es/design-patterns/singleton
https://refactoring.guru/es/design-patterns/singleton/python/example#lang-features


"""


from time import sleep
from datetime import datetime


class SingletonMeta(type):
    __instance__ = None

    def __call__(self):
        if self.__instance__ is None:
            self.__instance__ = super().__call__()
        return self.__instance__


class TimeSingleton(metaclass=SingletonMeta):
    def __init__(self):
        self.now_cls = datetime.utcnow()

    def now_method(self):
        return datetime.utcnow()


if __name__ == '__main__':
    s1 = TimeSingleton()
    print(s1.now_cls)
    print(s1.now_method())

    print("Esperando 3 seg")

    sleep(3)

    s2 = TimeSingleton()
    print(s2.now_cls)
    print(s2.now_method())
