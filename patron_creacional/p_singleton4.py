import urllib.parse
import urllib.request


class URLFetcher:
    def __init__(self):
        self.urls = []

    def fetch(self, url):
        req = urllib.request.Request(url)
        with urllib.request.urlopen(req) as response:
            if response.code == 200:
                the_page = response.read()
                print(the_page)

        urls = self.urls
        urls.append(url)
        self.urls = urls


def main1():
    f1 = URLFetcher()
    f2 = URLFetcher()
    print(f1 is f2)


class SingletonType(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(
                SingletonType, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class URLFetcher(metaclass=SingletonType):
    def fetch(self, url):
        req = urllib.request.Request(url)
        with urllib.request.urlopen(req) as response:
            if response.code == 200:
                the_page = response.read()
                print(the_page)

        urls = self.urls
        urls.append(url)
        self.urls = urls

    def dump_url_registry(self):
        return ", ".join(self.urls)


def main2():
    print(URLFetcher() is URLFetcher())


def main():
    MY_URLS = ['http://www.google.com', 'http://www.python.org']
    fetcher = URLFetcher()
    for url in MY_URLS:
        try:
            fetcher.fetch(url)
        except Exception as e:
            print(e)

    print("-"*5)
    done_urls = fetcher.dump_url_registry()
    print(f'Done URLs: {done_urls}')


if __name__ == "__main__":
    main1()
    main2()
    main()
