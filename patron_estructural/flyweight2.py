"""
Flyweight es un patrón de diseño estructural que te permite mantener más objetos dentro de la cantidad disponible de RAM
compartiendo las partes comunes del estado entre varios objetos en lugar de mantener toda la información en cada objeto.

https://refactoring.guru/es/design-patterns/flyweight

https://refactoring.guru/es/design-patterns/flyweight/python/example

https://stackabuse.com/structural-design-patterns-in-python#flyweight

"""


class BulletContext:
    def __init__(self, x, y, z, velocity):
        self.x = x
        self.y = y
        self.z = z
        self.velocity = velocity

 class BulletFlyweight:
    def __init__(self):
        self.asset = '■■►'
        self.bullets = []
        
    def bullet_factory(self, x, y, z, velocity):
        bull = [b for b in self.bullets if b.x==x and b.y==y and b.z==z and b.velocity==velocity]
        if not bull:
            bull = bullet(x,y,z,velocity)
            self.bullets.append(bull)
        else:
            bull = bull[0]
            
        return bull
        
    def print_bullets(self):
        print('Bullets:')
        for bullet in self.bullets:
            print(str(bullet.x)+' '+str(bullet.y)+' '+str(bullet.z)+' '+str(bullet.velocity))
            
            
if __name__ == '__main__':
    bf = BulletFlyweight()

    # adding bullets
    bf.bullet_factory(1,1,1,1)
    bf.bullet_factory(1,2,5,1)

    bf.print_bullets()
