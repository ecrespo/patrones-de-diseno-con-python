"""
Los patrones estructurales explican cómo ensamblar objetos y clases en estructuras más grandes, a la vez que se mantiene la flexibilidad y eficiencia de estas estructuras.

Adapter:
    es un patrón de diseño estructural que permite la colaboración entre objetos con interfaces incompatibles.

Problema : 
Imagina que estás creando una aplicación de monitoreo del mercado de valores. 
La aplicación descarga la información de bolsa desde varias fuentes en formato XML para presentarla al usuario con bonitos gráficos y diagramas.

En cierto momento, decides mejorar la aplicación integrando una inteligente biblioteca de análisis de una tercera persona. 
Pero hay una trampa: la biblioteca de análisis solo funciona con datos en formato JSON.

Intención: 
Unificar la interfaz de una clase con la de otra clase ya existente.
Esto permite usar la clase adaptada y la original de la misma forma haciendolas compatibles

Aplicabilidad: 
Se suele usar cuando queremos adaptar una librería para cubrir un caso y ya tenemos     una interfaz para el resto de los casos. 

También es útil para refactorizar un código en el que queremos sustituir una librería y es necesario que convivan las dos durante un tiempo. 

Esto nos permitirá usar ambas librerías de la misma forma hasta que eliminemos por completo la que queremos sustituir. 

Participantes: 
* Target: Clase o conjunto de clases a las que hay que adaptarse. 
* Adaptee: Clase que necesita ser adaptada. 
* Adapter: Clase que adaptará Adaptee a la interfaz de Target. 

https://refactoring.guru/es/design-patterns/adapter/python/example
https://stackabuse.com/structural-design-patterns-in-python#adapter
    
"""




class Target:
    """
    The Target defines the domain-specific interface used by the client code.
    """

    def request(self) -> str:
        return "Target: The default target's behavior."


class Adaptee:
    """
    The Adaptee contains some useful behavior, but its interface is incompatible
    with the existing client code. The Adaptee needs some adaptation before the
    client code can use it.
    """

    def specific_request(self) -> str:
        return ".eetpadA eht fo roivaheb laicepS"


class Adapter(Target):
    """
    The Adapter makes the Adaptee's interface compatible with the Target's
    interface via multiple inheritance.
    """

    def __init__(self, adaptee: Adaptee) -> None:
        self.adaptee = adaptee

    def request(self) -> str:
        return f"Adapter: (TRANSLATED) {self.adaptee.specific_request()[::-1]}"


if __name__ == "__main__":
    target = Target()
    adaptee = Adaptee()
    adapter = Adapter(adaptee)

    results = [target, adapter]
    for obj in results:
        print(obj.request())
