"""
Los patrones estructurales explican cómo ensamblar objetos y clases en estructuras más grandes, a la vez que se mantiene la flexibilidad y eficiencia de estas estructuras.

Adapter:
    es un patrón de diseño estructural que permite la colaboración entre objetos con interfaces incompatibles.

Problema : 
Imagina que estás creando una aplicación de monitoreo del mercado de valores. 
La aplicación descarga la información de bolsa desde varias fuentes en formato XML para presentarla al usuario con bonitos gráficos y diagramas.

En cierto momento, decides mejorar la aplicación integrando una inteligente biblioteca de análisis de una tercera persona. 
Pero hay una trampa: la biblioteca de análisis solo funciona con datos en formato JSON.

Intención: 
Unificar la interfaz de una clase con la de otra clase ya existente.
Esto permite usar la clase adaptada y la original de la misma forma haciendolas compatibles

Aplicabilidad: 
Se suele usar cuando queremos adaptar una librería para cubrir un caso y ya tenemos     una interfaz para el resto de los casos. 

También es útil para refactorizar un código en el que queremos sustituir una librería y es necesario que convivan las dos durante un tiempo. 

Esto nos permitirá usar ambas librerías de la misma forma hasta que eliminemos por completo la que queremos sustituir. 

Participantes: 
* Target: Clase o conjunto de clases a las que hay que adaptarse. 
* Adaptee: Clase que necesita ser adaptada. 
* Adapter: Clase que adaptará Adaptee a la interfaz de Target. 

https://refactoring.guru/es/design-patterns/adapter/python/example
https://stackabuse.com/structural-design-patterns-in-python#adapter
    
"""


from abc import ABC, abstractmethod


class PngInterface(ABC):
    @abstractmethod
    def draw(self):
        pass


class PngImage(PngInterface):
    def __init__(self, png):
        self.png = png
        self.format = "raster"

    def draw(self):
        print("drawing " + self.get_image())

    def get_image(self):
        return "png"


class SvgImage:
    def __init__(self, svg):
        self.svg = svg
        self.format = "vector"

    def get_image(self):
        return "svg"


class SvgAdapter(png_interface):
    def __init__(self, svg):
        self.svg = svg

    def rasterize(self):
        return "rasterized " + self.svg.get_image()

    def draw(self):
        img = self.rasterize()
        print("drawing " + img)


class ConvertingNonVector(Exception):
    # An exception used by class_adapter to check
    # whether an image can be rasterized
    pass


class ClassAdapter(png_image, svg_image):
    def __init__(self, image):
        self.image = image

    def rasterize(self):
        if(self.image.format == "vector"):
            return "rasterized " + self.image.get_image()
        else:
            raise ConvertingNonVector

    def draw(self):
        try:
            img = self.rasterize()
            print("drawing " + img)
        except ConvertingNonVector as e:
            print("drawing " + self.image.get_image())


if __name__ == '__main__':
    regular_png = PngImage("some data")
    regular_png.draw()

    example_svg = SvgImage("some data")
    example_adapter = SvgAdapter(example_svg)
    example_adapter.draw()


####################
    example_png = PngImage("some data")
    regular_png = ClassAdapter(example_png)
    regular_png.draw()

    example_svg = SvgImage("some data")
    example_adapter = ClassAdapter(example_svg)
    example_adapter.draw()
