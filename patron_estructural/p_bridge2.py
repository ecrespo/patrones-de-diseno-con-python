"""  
Patrón Bridge: es un patrón de diseño estructural que te permite dividir una clase grande, o un grupo de clases estrechamente relacionadas, 
en dos jerarquías separadas (abstracción e implementación) que pueden desarrollarse independientemente la una de la otra.

Digamos que tienes una clase geométrica Forma con un par de subclases: Círculo y Cuadrado. Deseas extender esta jerarquía de clase para 
que incorpore colores, por lo que planeas crear las subclases de forma Rojo y Azul. Sin embargo, como ya tienes dos subclases, 
tienes que crear cuatro combinaciones de clase, como CírculoAzul y CuadradoRojo.

Solución: 
Este problema se presenta porque intentamos extender las clases de forma en dos dimensiones independientes: por forma y por color. 
Es un problema muy habitual en la herencia de clases.

El patrón Bridge intenta resolver este problema pasando de la herencia a la composición del objeto. Esto quiere decir que se extrae una 
de las dimensiones a una jerarquía de clases separada, de modo que las clases originales referencian un objeto de la nueva jerarquía, 
en lugar de tener todo su estado y sus funcionalidades dentro de una clase.

https://refactoring.guru/es/design-patterns/bridge
https://refactoring.guru/es/design-patterns/bridge/python/example
https://stackabuse.com/structural-design-patterns-in-python#bridge

"""

from abc import ABC, abstractmethod


class Material(ABC):
    @abstractmethod
    def __str__(self):
        pass


class Cobblestone(Material):
    def __init__(self):
        pass

    def __str__(self):
        return 'cobblestone'


class Wood(Material):
    def __init__(self):
        pass

    def __str__(self):
        return 'wood'


class Building(ABC):
    @abstractmethod
    def print_name(self):
        pass


class Tower(Building):
    def __init__(self, name, material):
        self.name = name
        self.material = material

    def print_name(self):
        print(str(self.material) + ' tower ' + self.name)


class Mill(Building):
    def __init__(self, name, material):
        self.name = name
        self.material = material

    def print_name(self):
        print(str(self.material) + ' mill ' + self.name)


if __name__ == "__main__":
    cobb = Cobblestone()
    local_mill = Mill('Hilltop Mill', cobb)
    local_mill.print_name()

    wooden = Wood()
    watchtower = Tower('Abandoned Sentry', wooden)
    watchtower.print_name()
