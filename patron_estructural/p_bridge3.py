class ResourceContent:
    """
    Define la abstracción de la interface. 
    Mantiene una referencia a un objeto el cual representa al Implementor
    """

    def __init__(self, imp):
        self._imp = imp

    def show_content(self, path):
        self._imp, fetch(path)


class ResourceContentFetcher(metaclass=abc.ABCMeta):
    """
    Define la interface para la implentación de la clase fetch content.
    """
    @abc.abstractmethod
    def fetch(path):
        pass


class URLFetcher(ResouceContentFetcher):

    def fetch(path):
        req = urllib.request.Request(path)
        with urllib.request.urlopen(req) as response:
            if response.code == 200:
                the_page = response.read()
                print(the_page)


class LocalFileFetcher(ResourceContentFetcher):
    def fetch(self, path):
        with open(path) as f:
            print(f.read())


def main():
    url_fetcher = URLFetcher()
    iface = ResourceContent(url_fetcher)
    iface.show_content("http://www.python.org")
    print("="*8)
    localfs_fetcher = LocalFileFetcher()
    iface = ResourceContent(localfs_fetcher)
    iface.show_content('file.txt')
