"""
Composite es un patrón de diseño estructural que te permite componer objetos en estructuras de árbol 
y trabajar con esas estructuras como si fueran objetos individuales.

Problema
El uso del patrón Composite sólo tiene sentido cuando el modelo central de tu aplicación puede representarse en forma de árbol.

Por ejemplo, imagina que tienes dos tipos de objetos: Productos y Cajas. Una Caja puede contener varios Productos así como cierto 
número de Cajas más pequeñas. Estas Cajas pequeñas también pueden contener algunos Productos o incluso Cajas más pequeñas, y así sucesivamente.

Digamos que decides crear un sistema de pedidos que utiliza estas clases. Los pedidos pueden contener productos sencillos sin envolver, 
así como cajas llenas de productos... y otras cajas. ¿Cómo determinarás el precio total de ese pedido?


Solución
El patrón Composite sugiere que trabajes con Productos y Cajas a través de una interfaz común que declara un método para calcular el precio total.

¿Cómo funcionaría este método? Para un producto, sencillamente devuelve el precio del producto. Para una caja, recorre cada artículo que 
contiene la caja, pregunta su precio y devuelve un total por la caja. Si uno de esos artículos fuera una caja más pequeña, esa caja 
también comenzaría a repasar su contenido y así sucesivamente, hasta que se calcule el precio de todos los componentes internos. Una caja 
podría incluso añadir costos adicionales al precio final, como costos de empaquetado.

https://refactoring.guru/es/design-patterns/composite
https://refactoring.guru/es/design-patterns/composite/python/example
https://stackabuse.com/structural-design-patterns-in-python#composite
"""

from abc import ABC, abstractmethod


class Item(ABC):
    @abstractmethod
    def return_price(self):
        pass


class Box(Item):
    def __init__(self, contents):
        self.contents = contents

    def return_price(self):
        price = 0
        for item in self.contents:
            price = price + item.return_price()
        return price


class Phone(Item):
    def __init__(self, price):
        self.price = price

    def return_price(self):
        return self.price


class Charger(Item):
    def __init__(self, price):
        self.price = price

    def return_price(self):
        return self.price


class Earphones(Item):
    def __init__(self, price):
        self.price = price

    def return_price(self):
        return self.price


if __name__ == "__main__":
    phone_case_contents = []
    phone_case_contents.append(Phone(200))
    phone_case_box = Box(phone_case_contents)

    big_box_contents = []
    big_box_contents.append(phone_case_box)
    big_box_contents.append(Charger(10))
    big_box_contents.append(Earphones(10))
    big_box = Box(big_box_contents)

    print("Total price: " + str(big_box.return_price()))
