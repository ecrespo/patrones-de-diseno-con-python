"""Decorator: Extender la funcionalidad de un método, función o clase dinámicamente sin sobreescribir la función o crear una subclase. 
    Aplicabilidad: 
    Se suele usar cuando quieres extender una función o un método añadiendo funcionalidades antes o después de su ejecución. 
    Warning -> También se puede usar para modificar los parámetros de entrada o impedir que se ejecute. 
    Participantes: 
    ->    * Component: Interfaz para los objetos a los que se le puede extender funcionalidades dinámicas. 
        * ConcreteComponent: Objeto que se extenderá dinámicamente 
    ->    * Decorator: Define una interfaz sobre Component con los elementos que se pueden definir dinámicamente. 
        * ConcreteDecorator: Objeto que extiende los ConcreteComponents. 
   
Decorator es un patrón de diseño estructural que te permite añadir funcionalidades a objetos colocando estos objetos dentro de objetos encapsuladores especiales que contienen estas funcionalidades.
https://refactoring.guru/es/design-patterns/decorator
https://refactoring.guru/es/design-patterns/decorator/python/example   
https://stackabuse.com/structural-design-patterns-in-python#decorator         
"""


import functools


def my_decorator_one(func):
    def wrapper(*args, **kwargs):
        prev_op = "my decorator one("
        mid_op = func(*args, **kwargs)
        next_op = ")"
        return prev_op + mid_op + next_op

    return wrapper


def my_decorator_two(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        prev_op = "my decorator two("
        mid_op = func(*args, **kwargs)
        next_op = ")"
        return prev_op + mid_op + next_op

    return wrapper


@my_decorator_one
def greet(num=1, msg=""):
    return "Hola mundo!"


@my_decorator_two
def magic():
    return "es una prueba loca"


@my_decorator_one
@my_decorator_two
def emoji():
    return ":-) "


if __name__ == '__main__':
    print("function name: ", greet.__name__,)
    print("result: ", greet())
    print("\n")
    print("function name: ", magic.__name__,)
    print("result: ", magic())
    print("\n")
    print("function name: ", emoji.__name__,)
    print("result: ", emoji())
    print("\n")
