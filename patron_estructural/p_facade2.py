"""
Facade es un patrón de diseño estructural que proporciona una interfaz simplificada a una biblioteca,
un framework o cualquier otro grupo complejo de clases.

Problema
Imagina que debes lograr que tu código trabaje con un amplio grupo de objetos que pertenecen a una sofisticada biblioteca o framework.
Normalmente, debes inicializar todos esos objetos, llevar un registro de las dependencias, ejecutar los métodos en el orden correcto y
así sucesivamente.

Como resultado, la lógica de negocio de tus clases se vería estrechamente acoplada a los detalles de implementación de las clases de
terceros, haciéndola difícil de comprender y mantener.

https://refactoring.guru/es/design-patterns/facade


Cuando llamas a una tienda para hacer un pedido por teléfono, un operador es tu fachada a todos los servicios y departamentos de la tienda.
El operador te proporciona una sencilla interfaz de voz al sistema de pedidos, pasarelas de pago y varios servicios de entrega.

https://stackabuse.com/structural-design-patterns-in-python#facade

https://refactoring.guru/es/design-patterns/facade/python/example
"""


class BulletContext:
    def __init__(self, x, y, z, velocity):
        self.x = x
        self.y = y
        self.z = z
        self.velocity = velocity

 class BulletFlyweight:
    def __init__(self):
        self.asset = '■■►'
        self.bullets = []
        
    def bullet_factory(self, x, y, z, velocity):
        bull = [b for b in self.bullets if b.x==x and b.y==y and b.z==z and b.velocity==velocity]
        if not bull:
            bull = bullet(x,y,z,velocity)
            self.bullets.append(bull)
        else:
            bull = bull[0]
            
        return bull
        
    def print_bullets(self):
        print('Bullets:')
        for bullet in self.bullets:
            print(str(bullet.x)+' '+str(bullet.y)+' '+str(bullet.z)+' '+str(bullet.velocity))
            



if __name__ == '__main__':
    bf = BulletFlyweight()

    # adding bullets
    bf.bullet_factory(1,1,1,1)
    bf.bullet_factory(1,2,5,1)

    bf.print_bullets()
